/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graf;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Stack;

/**
 *
 * @author gustaf.lonn
 */
public class GrafTest {

    public static void visaMatris(Double[][] grannmatris) {
        for (int i = 0; i < grannmatris.length; i++) {
            for (int j = 0; j < grannmatris[0].length; j++) {
                System.out.format("%3.0f ", grannmatris[i][j]);
            }
            System.out.print("\n");
        }
    }

    public static Graf lasFil(String fil) {
        Graf graf = new Graf();
        BufferedReader reader = null;
        FileInputStream fileIn = null;
        try {
            fileIn = new FileInputStream(fil);
            reader = new BufferedReader(new InputStreamReader(fileIn));
            int x = Integer.parseInt(reader.readLine());
            for (int i = 0; i < x; i++) {
                graf.laggTillNod(reader.readLine());
            }
            String line = reader.readLine();
            while (line != null) {
                String[] temp = line.split(" ");
                graf.laggTillBage(temp[0], temp[1], Double.parseDouble(temp[2]));
                line = reader.readLine();
            }
        } catch (Exception e) {
            System.out.println("it goofed");
        }
        return graf;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Graf graf = lasFil("data.txt");
        visaMatris(graf.geGrannmatris());
        System.out.println("");
        String[] dj = graf.dijkstra("Kuopio", "Esbo");
        Double l = graf.langd(dj);
        System.out.println(Arrays.toString(dj) + " " + l.toString() + "\n");

        Graf graf2 = lasFil("data_test10.txt");
        visaMatris(graf2.geGrannmatris());
        System.out.println("");
        long then = System.nanoTime();
        graf2.handelsresande(graf2.geNoderArray(), 0, graf2.geNoder().toArray().length-1, "Åbo", "Vasa");
        System.out.println((System.nanoTime()-then)/1e9d);
    }
}
