/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;

/**
 *
 * @author gustaf.lonn
 */
public class Graf<T> {

    private ArrayList<T> noder;
    private HashMap<Set<T>, Double> bagar;
    private Double kortaste_rutt = Double.POSITIVE_INFINITY;

    Graf() {
        noder = new ArrayList<T>();
        bagar = new HashMap<Set<T>, Double>();
    }

    public ArrayList<T> geNoder() {
        return this.noder;
    }
    
    public String[] geNoderArray(){
        String arr[] = new String[this.noder.size()];
        for (int i = 0; i < this.noder.size(); i++) {
            arr[i] = this.noder.get(i).toString();
        }
        return arr;
    }

    public void laggTillNod(T nod) {
        noder.add(nod);
    }

    public void laggTillBage(T nod1, T nod2) {
        this.laggTillBage(nod1, nod2, 1d);
    }

    public void laggTillBage(T nod1, T nod2, Double vikt) {
        HashSet<T> bage = new HashSet<>();
        bage.add(nod1);
        bage.add(nod2);
        bagar.put(bage, vikt);
    }

    public Double[][] geGrannmatris() {
        int antalNoder = noder.size();
        Double[][] grannmatris = new Double[antalNoder][antalNoder];
        for (int i = 0; i < antalNoder; i++) {
            for (int j = 0; j < antalNoder; j++) {
                grannmatris[i][j] = 0d;
            }
        }
        for (Set<T> minBage : bagar.keySet()) {
            Iterator<T> it = minBage.iterator();
            int index1 = noder.indexOf(it.next());
            int index2 = noder.indexOf(it.next());
            grannmatris[index1][index2] = bagar.get(minBage);
            grannmatris[index2][index1] = bagar.get(minBage);
        }
        return grannmatris;
    }

    public ArrayList<T> geGrannar(T nod) {
        ArrayList<T> grannar = new ArrayList<>();
        for (Set<T> bage : bagar.keySet()) {
            if (bage.contains(nod)) {
                Iterator<T> it = bage.iterator();
                T forsta = it.next();
                if (nod.equals(forsta)) {
                    grannar.add(it.next());
                } else {
                    grannar.add(forsta);
                }
            }
        }
        //System.out.println(grannar);
        return grannar;
    }

    public Double vikt(T nod1, T nod2) {
        HashSet<T> bage = new HashSet<T>();
        bage.add(nod1);
        bage.add(nod2);
        return bagar.getOrDefault(bage, 0d);
    }

    public String[] dijkstra(T source, T target) {
        ArrayList<T> Q = new ArrayList<>(); // Has to start by containing all nodes (unvisited)
        HashMap<T, Double> dist = new HashMap<>();
        HashMap<T, T> prev = new HashMap<>();
        Stack<T> s = new Stack<>();
        for (T v : this.noder) {

            dist.put(v, Double.POSITIVE_INFINITY);
            prev.put(v, null);

            Q.add(v);
        }

        dist.put(source, 0d);

        while (Q.size() > 0) {
            T u;
            Entry<T, Double> min = null;
            for (Entry<T, Double> entry : dist.entrySet()) {
                if (min == null || min.getValue() > entry.getValue()) {
                    if (Q.contains(entry.getKey())) {
                        min = entry;
                    }
                }
            }
            u = min.getKey();
            Q.remove(u);
            if (u.equals(target)) {

                u = target;
                while (prev.get(u) != null) {
                    s.push(u);
                    u = prev.get(u);
                }
                s.push(u);
            }
            for (T neighbor_v : this.geGrannar(u)) {
                if (Q.contains(neighbor_v)) {
                    double alt = dist.get(u) + vikt(neighbor_v, u);
                    if (alt < dist.get(neighbor_v)) {
                        dist.put(neighbor_v, alt);
                        prev.put(neighbor_v, u);
                    }
                }
            }
        }
        String rutt[] = new String[s.size()];
        for (int i = 0; i < rutt.length; i++) {
            rutt[i] = s.pop().toString();
        }
        return rutt;
    }

    public double langd(String[] s) {
        String prev = null;
        double langd = 0;
        for (String nod : s) {
            if (prev != null) {
                langd += this.vikt((T) prev, (T) nod);
            }
            prev = nod;
        }
        return langd;
    }
    public void handelsresande(String[] arr, int start_idx, int end_idx, String source, String target) {
        int i, tempDist;
        ArrayList<Integer> rutt = new ArrayList<>();
        for (i = start_idx; i <= end_idx; i++) {
            String temp = "";
            temp = arr[i];
            arr[i] = arr[start_idx];
            arr[start_idx] = temp;
            rutt(arr, start_idx + 1, end_idx, source, target);
            handelsresande(arr, start_idx + 1, end_idx, source, target);
            temp = arr[i];
            arr[i] = arr[start_idx];
            arr[start_idx] = temp;
        }
    }
    public void rutt(String[] arr, int start_idx, int end_idx, String source, String target){
        boolean counting = false;
        Double tempRuttLangd = 0d;
        String tempRutt = "";
        for (int i = 0; i < arr.length; i++) {
            if (counting) {
                tempRutt += arr[i] + " ";
                tempRuttLangd += vikt((T)arr[i-1], (T)arr[i]);
            }
            if ((arr[i].equals(source) || arr[i].equals(target)) && !counting) {
                counting = true;
                tempRutt += arr[i] + " ";
            } else if ((arr[i].equals(source) || arr[i].equals(target)) && counting && tempRuttLangd < this.kortaste_rutt) {
                System.out.println("Ny kortaste rutt: " + tempRutt + "km: " + tempRuttLangd);
                this.kortaste_rutt = tempRuttLangd;
            }
        }
    }
}
