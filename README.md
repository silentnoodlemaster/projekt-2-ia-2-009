# Projekt 2

## Graph project

### Read Graph part
the function `lasFil` takes a string, the filename, as input and returns a graph.

That file is then opened and parsed as a graph and is returned. 
### Dijkstra's Algo
The funktion `dijkstra` takes two nodes as input, in this case Strings. And returns a array of strings with the route.

the `langd` function takes the array you get from the previous function and returns the lenght of travel as Double.
### TSP
`handelsresande` is bruteforcing the path as per assignment, we scaled down the data because we discovered that it would have taken ca. 5 hours to finish. so a full set of 15 nodes would have taken roughly 45 days.

```
n = number of nodes
O = speed of single node processing in second
O = 0.00000305 on Busters laptoo

time in seconds = O(n!)
```

---
*2018 Ben Lönnqvist & Buster Pihl*